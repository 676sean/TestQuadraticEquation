#include "stdafx.h">
#include "math.h"
#include "iostream"
#include "HelloFunctions.h"


int Dis(double a, double b, double c)
{
	double d;
	d = b*b - 4 * a*c;	
	return d;
}
int quad (double a, double b, double c)
{
	double disk, x1, x2;
	disk = Dis(a, b, c);
	//cout << disk << "\n";
	if (disk < 0)
	{
		//cout << "Error\n";
	}
	else
	{
		if (disk == 0)
		{
			x1 = -b / (2 * a);
			//cout << x1;
			return x1;
		}
		else
		{

			x1 = -b / (2 * a) - (sqrt(disk)) / (2 * a);
			x2 = -b / (2 * a) + (sqrt(disk)) / (2 * a);
			//cout << x1 << " " << x2;
			return x1;
		}
	}
}

