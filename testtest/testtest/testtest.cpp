#include "stdafx.h">
#include <iostream>
#include "HelloFunctions.h"

using namespace std;

#include "gtest/gtest.h"

class CDistest : public ::testing::Test {
};

TEST_F(CDistest, CheckPer)
{

	ASSERT_TRUE(Dis(2, 11, 5) == 81);
	EXPECT_TRUE(quad(2, 11, 5) == -5);

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
	std::getchar();
}
